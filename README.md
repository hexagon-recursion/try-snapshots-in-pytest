I want to try the following libraries to see which fits my current needs best:

# SnapshotTest

* https://pypi.org/project/snapshottest/
* `requires_python=">=3.5"`
* To update snapshots run: `pytest --snapshot-update`
* Supports pytest.mark.parametrize

Example test:

```py
def test_mything(snapshot):
    """Example snapshot test"""
    snapshot.assert_match('Hello, snapshot')
```

Example failure:

```py
=================================== FAILURES ===================================
_________________________________ test_mything _________________________________

snapshot = <snapshottest.pytest.PyTestSnapshotTest object at 0x7f9c5c1b5970>

    def test_mything(snapshot):
        """Example snapshot test"""
>       snapshot.assert_match('Hello, updated snapshot')

test_sample.py:3: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
.tox/py3/lib/python3.9/site-packages/snapshottest/module.py:247: in assert_match
    self.assert_value_matches_snapshot(value, prev_snapshot)
.tox/py3/lib/python3.9/site-packages/snapshottest/module.py:230: in assert_value_matches_snapshot
    formatter.assert_value_matches_snapshot(self, test_value, snapshot_value, Formatter())
.tox/py3/lib/python3.9/site-packages/snapshottest/formatters.py:20: in assert_value_matches_snapshot
    test.assert_equals(formatter.normalize(test_value), snapshot_value)
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 

self = <snapshottest.pytest.PyTestSnapshotTest object at 0x7f9c5c1b5970>
value = 'Hello, updated snapshot', snapshot = 'Hello, snapshot'

    def assert_equals(self, value, snapshot):
>       assert value == snapshot
E       AssertionError: assert 'Hello, updated snapshot' == 'Hello, snapshot'
E         - Hello, snapshot
E         + Hello, updated snapshot
E         ?       ++++++++

.tox/py3/lib/python3.9/site-packages/snapshottest/module.py:233: AssertionError
============================= SnapshotTest summary =============================
1 snapshots failed in 1 test suites. Inspect your code or run with `pytest --snapshot-update` to update them.
=========================== short test summary info ============================
FAILED test_sample.py::test_mything - AssertionError: assert 'Hello, updated ...
============================== 1 failed in 0.10s ===============================
```

Example diff (showing snapshot diff only):
```diff
diff --git a/snapshottest/snapshots/snap_test_sample.py b/snapshottest/snapshots/snap_test_sample.py
index dfca20f..298c817 100644
--- a/snapshottest/snapshots/snap_test_sample.py
+++ b/snapshottest/snapshots/snap_test_sample.py
@@ -7,4 +7,4 @@ from snapshottest import Snapshot
 
 snapshots = Snapshot()
 
-snapshots['test_mything 1'] = 'Hello, snapshot'
+snapshots['test_mything 1'] = 'Hello, updated snapshot'
```

Supports pytest.mark.parametrize:

```diff
diff --git a/snapshottest/snapshots/snap_test_sample.py b/snapshottest/snapshots/snap_test_sample.py
index 298c817..74d5331 100644
--- a/snapshottest/snapshots/snap_test_sample.py
+++ b/snapshottest/snapshots/snap_test_sample.py
@@ -7,4 +7,8 @@ from snapshottest import Snapshot
 
 snapshots = Snapshot()
 
+snapshots['test_function_output_with_snapshot[bar] 1'] = 'bar'
+
+snapshots['test_function_output_with_snapshot[foo] 1'] = 'foo'
+
 snapshots['test_mything 1'] = 'Hello, updated snapshot'
diff --git a/snapshottest/test_sample.py b/snapshottest/test_sample.py
index 110b4f0..1085aae 100644
--- a/snapshottest/test_sample.py
+++ b/snapshottest/test_sample.py
@@ -1,3 +1,5 @@
-def test_mything(snapshot):
-    """Example snapshot test"""
-    snapshot.assert_match('Hello, updated snapshot')
+import pytest
+
+@pytest.mark.parametrize("param", ["foo", "bar"])
+def test_function_output_with_snapshot(param, snapshot):
+    snapshot.assert_match(param)
```

# pytest-snapshot

* https://pypi.org/project/pytest-snapshot/
* Requires: Python >=3.5
* To create or update snapshots run: `pytest --snapshot-update`
* Supports pytest.mark.parametrize

Example test:

```py
def test_function_output_with_snapshot(snapshot):
    snapshot.assert_match('hello, test\n', 'foo_output.txt')
```

Example failure:
```py
test_sample.py F                                                                                                                                                   [100%]

================================================================================ FAILURES ================================================================================
___________________________________________________________________ test_function_output_with_snapshot ___________________________________________________________________

snapshot = <pytest_snapshot.plugin.Snapshot object at 0x79e4b601e970>

    def test_function_output_with_snapshot(snapshot):
>       snapshot.assert_match('hello, updated test\n', 'foo_output.txt')

test_sample.py:2: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 

self = <pytest_snapshot.plugin.Snapshot object at 0x79e4b601e970>, value = 'hello, updated test\n', snapshot_name = 'foo_output.txt'

    def assert_match(self, value: Union[str, bytes], snapshot_name: Union[str, Path]):
        """
        Asserts that ``value`` equals the current value of the snapshot with the given ``snapshot_name``.
    
        If pytest was run with the --snapshot-update flag, the snapshot will instead be updated to ``value``.
        The test will fail if there were any changes to the snapshot.
        """
        compare, encode, decode = self._get_compare_encode_decode(value)
        snapshot_path = self._snapshot_path(snapshot_name)
    
        if snapshot_path.is_file():
            encoded_expected_value = snapshot_path.read_bytes()
        elif snapshot_path.exists():
            raise AssertionError('snapshot exists but is not a file: {}'.format(shorten_path(snapshot_path)))
        else:
            encoded_expected_value = None
    
        if self._snapshot_update:
            encoded_value = encode(value)
            if encoded_expected_value is None or encoded_value != encoded_expected_value:
                decoded_encoded_value = decode(encoded_value)
                if decoded_encoded_value != value:
                    raise ValueError("value is not supported by pytest-snapshot's serializer.")
    
                snapshot_path.parent.mkdir(parents=True, exist_ok=True)
                snapshot_path.write_bytes(encoded_value)
                if encoded_expected_value is None:
                    self._created_snapshots.append(snapshot_path)
                else:
                    self._updated_snapshots.append(snapshot_path)
        else:
            if encoded_expected_value is not None:
                expected_value = decode(encoded_expected_value)
                try:
                    compare(value, expected_value)
                except AssertionError as e:
                    snapshot_diff_msg = str(e)
                else:
                    snapshot_diff_msg = None
    
                if snapshot_diff_msg is not None:
                    snapshot_diff_msg = 'value does not match the expected value in snapshot {}\n{}'.format(
                        shorten_path(snapshot_path), snapshot_diff_msg)
>                   raise AssertionError(snapshot_diff_msg)
E                   AssertionError: value does not match the expected value in snapshot snapshots/test_sample/test_function_output_with_snapshot/foo_output.txt
E                   assert 'hello, updated test\n' == 'hello, test\n'
E                     - hello, test
E                     + hello, updated test
E                     ?        ++++++++

.tox/py3/lib/python3.9/site-packages/pytest_snapshot/plugin.py:194: AssertionError
======================================================================== short test summary info =========================================================================
FAILED test_sample.py::test_function_output_with_snapshot - AssertionError: value does not match the expected value in snapshot snapshots/test_sample/test_function_out...
=========================================================================== 1 failed in 0.07s ============================================================================
```

Example diff

```diff
diff --git a/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/foo_output.txt b/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/foo_output.txt
index 2da5b12..703ae5d 100644
--- a/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/foo_output.txt
+++ b/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/foo_output.txt
@@ -1 +1 @@
-hello, test
+hello, updated test
```

Supports pytest.mark.parametrize:

```diff
diff --git a/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/bar/foo_output.txt b/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/bar/foo_output.txt
new file mode 100644
index 0000000..ba0e162
--- /dev/null
+++ b/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/bar/foo_output.txt
@@ -0,0 +1 @@
+bar
\ No newline at end of file
diff --git a/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/foo/foo_output.txt b/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/foo/foo_output.txt
new file mode 100644
index 0000000..1910281
--- /dev/null
+++ b/pytest-snapshot/snapshots/test_sample/test_function_output_with_snapshot/foo/foo_output.txt
@@ -0,0 +1 @@
+foo
\ No newline at end of file
diff --git a/pytest-snapshot/test_sample.py b/pytest-snapshot/test_sample.py
index 8d66114..f4b0f5b 100644
--- a/pytest-snapshot/test_sample.py
+++ b/pytest-snapshot/test_sample.py
@@ -1,2 +1,5 @@
-def test_function_output_with_snapshot(snapshot):
-    snapshot.assert_match('hello, updated test\n', 'foo_output.txt')
+import pytest
+
+@pytest.mark.parametrize("param", ["foo", "bar"])
+def test_function_output_with_snapshot(param, snapshot):
+    snapshot.assert_match(param, 'foo_output.txt')
```

# pytest-insta

* https://pypi.org/project/pytest-insta/
* Requires: Python >=3.8 - probably not the right choice for borg-backup
