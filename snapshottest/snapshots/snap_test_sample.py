# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_function_output_with_snapshot[bar] 1'] = 'bar'

snapshots['test_function_output_with_snapshot[foo] 1'] = 'foo'

snapshots['test_mything 1'] = 'Hello, updated snapshot'
