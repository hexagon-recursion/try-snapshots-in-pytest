import pytest

@pytest.mark.parametrize("param", ["foo", "bar"])
def test_function_output_with_snapshot(param, snapshot):
    snapshot.assert_match(param)
